# coding=utf-8
from django import forms
from django.contrib.auth import get_user_model
from django_select2.forms import ModelSelect2Widget

from users import utils
from users.models import UserProfile

User = get_user_model()


class UserChoiceWidget(ModelSelect2Widget):
    """Select2 widget для выбора пользователя"""
    model = User

    # прескорбное дублирование ключей
    # с utils.get_users_by_username_or_tax_id :/
    # todo
    search_fields = [
        'username__istartswith',
        'profile__tax_identifier__istartswith',
    ]


class UserChoiceField(forms.ModelChoiceField):
    """Поле для выбора пользователя"""
    widget = UserChoiceWidget(
        data_view='users:search',
        attrs={'data-minimum-input-length': 3},
    )

    def __init__(self, *args, **kwargs):
        kwargs['queryset'] = User.objects.all()
        super(UserChoiceField, self).__init__(*args, **kwargs)


class MoneyTransferForm(forms.Form):
    """Форма перевода средств"""
    user_src = UserChoiceField(
        required=True,
        label=u"Пользователь",
        help_text=u"Пользователь, со счета которого переводятся средства",
    )

    tax_id_dst = forms.CharField(
        label=u"ИНН получателей",
        help_text=u"ИНН пользователей, кому предназначается перевод"
    )

    amount = forms.DecimalField(
        label=u"Сумма",
        help_text=u"Количество денег, которое необходимо перевести",
        max_digits=15, decimal_places=2, min_value=1.0, initial=1.0,
    )

    @staticmethod
    def check_enough_balance(user_src, amount):
        """
        Проверяет, что баланса хватит
        :param user_src:
        :param amount:
        :return:
        """
        if not utils.has_enough_balance(user_src, amount):
            raise forms.ValidationError(
                u"У пользователя %s недостаточно средств" % user_src.username,
                code=u"not_enough_balance"
            )

    def clean_amount(self):
        """
        Проверяет достаточность баланса при условии, что пользователь
        выбран корректно
        """
        user_src = self.cleaned_data.get('user_src', None)

        if not user_src:
            return

        user_src = utils.get_user(user_src)
        amount = self.cleaned_data.get('amount', None)

        if user_src:
            self.check_enough_balance(user_src, amount)

        return amount

    def clean_tax_id_dst(self):
        """Проверяет существование пользователя с указанным ИННН"""
        tax_id_dst = self.cleaned_data.get('tax_id_dst', None)

        if not UserProfile.objects.filter(tax_identifier=tax_id_dst).count():
            raise forms.ValidationError(
                u"Пользователей с ИНН %s не существует" % tax_id_dst,
                u"no_user"
            )

        return tax_id_dst
