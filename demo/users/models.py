# coding=utf-8
from django.db import models


class UserProfile(models.Model):
    """
    Кастомная модель пользователя с доп. полями
    """
    user = models.OneToOneField(
        'auth.User', on_delete=models.CASCADE, related_name='profile'
    )
    tax_identifier = models.CharField(
        max_length=12,
        verbose_name=u"ИНН",
        help_text=u"ИНН",
        blank=True,
        null=True,
        db_index=True,
    )
    balance_rub = models.DecimalField(
        verbose_name=u"Счет (руб.)",
        max_digits=15,
        decimal_places=2,
        default=0.0
    )
