# coding=utf-8
import itertools
from decimal import Decimal

from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models import Q

from .models import UserProfile

User = get_user_model()


def get_user(value):
    """
    Возвращает пользователя по val,
    где val может быть объектом User, UserProfile, числом или строкой

    NB: вообще, этому тут не место, но простоты ради оставим как есть
    :param value: id|User
    """
    if isinstance(value, (list, tuple, set)):
        return get_users(value)

    if isinstance(value, User):
        return value

    if isinstance(value, UserProfile):
        return value.user

    return User.objects.prefetch_related('profile').get(id=value)


def get_users(value):
    """
    Возвращает список пользователей по val,
    где val может быть списком User, UserProfile, числом или строкой

    NB: вообще, этому тут не место, но простоты ради оставим как есть
    :param value: seq
    :return:
    """
    if not isinstance(value, (list, tuple, set)):
        value = [value]

    return map(get_user, value)


def get_users_by_tax_id(val):
    """
    Возвращает список пользователей с указанным ИНН
    :param val:
    :return:
    """
    return User.objects.prefetch_related('profile').filter(
        profile__tax_identifier=val
    )


@transaction.atomic
def handle_money_transfer(user_src, user_dst, amount):
    """
    Переводит деньги
    :param user_src:
    :param user_dst:
    :param amount:
    :return:
    """
    # перед непосредственно переводом средств следует проверить, а существуют
    # ли в ДАННЫЙ момент пользователи, кому мы хотим перевести средства
    # NB: на этом месте может быть любая другая проверка из предметной области,
    # которая может препятствовать переводу
    if User.objects.filter(id__in=[user_src.id, user_dst.id]).count() != 2:
        return False

    amount = Decimal(amount)

    src_balance = Decimal(user_src.profile.balance_rub)
    dst_balance = Decimal(user_dst.profile.balance_rub)

    user_src.profile.balance_rub = src_balance - amount
    user_dst.profile.balance_rub = dst_balance + amount

    user_src.profile.save()
    user_dst.profile.save()

    return user_src.profile.balance_rub < src_balance and \
        user_dst.profile.balance_rub > dst_balance


def handle_bulk_money_transfer(user_src, users_dst, amount):
    """
    Переводит сумму amount от пользователя user_src пользователям users_dst
    :param user_src:
    :param users_dst:
    :param amount:
    :return:
    """
    if not users_dst:
        return  # bla bla bla

    amount_div = float(amount) / len(users_dst)
    users_dst = sorted(users_dst, key=lambda o: o.id)

    transfer_results = map(
        lambda dst: handle_money_transfer(user_src, dst, amount_div),
        users_dst
    )

    return dict(itertools.izip(users_dst, transfer_results))


def has_enough_balance(user, amount):
    """
    Проверяет достаточность средств
    :param user:
    :param amount:
    :return:
    """
    user = get_user(user)
    return user.profile.balance_rub >= amount


def get_users_by_username_or_tax_id(value):
    """
    Возвращает пользователей с указанным username или ИНН
    :param value:
    """
    return User.objects.filter(
        Q(username__istartswith=value) |
        Q(profile__tax_identifier__istartswith=value)
    ).all()
