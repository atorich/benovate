(function ($) {

    var STATE_READY = 'ready';
    var STATE_PENDING = 'pending';

    $.fn.ajaxForm = function (options) {
        var $self = $(this),
            settings = $.extend({
                dataType: 'html',
                yaCounterTarget: null,
                yaCounterTargetParams: null,
                success: function () {
                },
                error: function () {
                },
                onDuplicate: function () {
                } // попытка отправить повторно, пока предыдущий запрос не завершен
            }, options);

        var state = STATE_READY;

        var onSubmit = function (e) {
            var $form = $(this);

            e.preventDefault();

            if (state != STATE_READY) {
                settings.onDuplicate.apply();
                return;
            }

            state = STATE_PENDING;

            $.ajax({
                url: $form.attr('action'),
                data: $form.serialize(),
                type: 'post',
                dataType: 'html',
                success: function () {
                    try {
                        $form.get(0).reset();
                        settings.success.apply(this, arguments);
                    } catch (error) {
                        throw error
                    } finally {
                        setTimeout(function() {
                            state = STATE_READY;
                        }, 300);
                    }
                },
                error: function () {
                    try {
                        settings.error.apply(this, arguments);
                    } catch (error) {
                        throw error;
                    } finally {
                        setTimeout(function() {
                            state = STATE_READY;
                        }, 300);
                    }
                }
            })
        };

        var methods = {
            init: function() {
                $self.on('submit', onSubmit);
            },

            addSuccessCallback: function(callback) {
                settings.success = callback;
            }
        };

        if (arguments.length <= 1) {
            //only settings passed
            methods.init.call();
        }

        if (arguments.length > 1) {
            //method & params passed - proxy to called method
            var methodName = arguments[0];
            var methodArguments = Array.prototype.slice.call(arguments, 1);
            var method = methods[methodName];

            method.apply(this, methodArguments);
        }
    }
})(jQuery);