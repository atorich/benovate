from django.conf.urls import url, patterns

from users import views


urlpatterns = patterns(
    '',
    url(r'^transfer/$', views.TransferFormView.as_view(), name='transfer'),
    url(r'^search/$', views.user_search_view, name='search'),
)
