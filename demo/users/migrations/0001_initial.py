# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tax_identifier', models.CharField(help_text='\u0418\u041d\u041d', max_length=12, null=True, verbose_name='\u0418\u041d\u041d', blank=True)),
                ('balance_rub', models.DecimalField(default=0.0, verbose_name='\u0421\u0447\u0435\u0442 (\u0440\u0443\u0431.)', max_digits=15, decimal_places=2)),
                ('user', models.OneToOneField(on_delete=b'CASCADE', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
