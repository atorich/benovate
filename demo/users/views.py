# coding=utf-8
import json
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic import FormView

from users import forms, utils

User = get_user_model()


class TransferFormView(FormView):
    """Обрабатывает перевод средств"""
    form_class = forms.MoneyTransferForm
    template_name = u'users/transfer/form.html'

    SUCCESS_MESSAGE = u'Средства успешно переведены'
    PARTIAL_FAILED_MESSAGE = u'Переведена лишь часть средств. Пользователи, ' \
                             u'не получившие перевод: {}'
    FORM_INVALID_MESSAGE = u'Ошибка заполнения формы'

    def form_valid(self, form):
        user_src = form.cleaned_data.get(u'user_src', None)
        users_dst = utils.get_users_by_tax_id(
            form.cleaned_data.get(u'tax_id_dst', None)
        )
        amount = form.cleaned_data.get(u'amount', None)

        rv = utils.handle_bulk_money_transfer(user_src, users_dst, amount)

        if all(rv.values()):
            messages.success(self.request, self.SUCCESS_MESSAGE)
        else:
            failed_users = [u.username for u in rv.keys()]
            messages.warning(
                self.request, self.PARTIAL_FAILED_MESSAGE.format(
                    ', '.join(failed_users)
                )
            )
        return redirect(reverse(u'users:transfer'))

    def form_invalid(self, form):
        messages.error(self.request, self.FORM_INVALID_MESSAGE)
        return super(TransferFormView, self).form_invalid(form)


def user_search_view(request):
    """Обрабатывает поиск пользователя по username или tax_id"""
    term = request.GET.get('term', '')

    if len(term) > 2:
        objects = [
            {'id': o.id, 'text': o.username}
            for o in utils.get_users_by_username_or_tax_id(term)
        ]
    else:
        objects = []

    return HttpResponse(
        json.dumps({'err': 'nil', 'results': objects}),
        content_type='application/json'
    )
