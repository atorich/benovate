# coding=utf-8
import json

from autofixture import AutoFixture
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase
from mock import MagicMock

from users import views
from users import utils, models, forms

User = get_user_model()


class MoneyTransferTestCase(TestCase):
    """Тестирует логику перевода средств"""
    def setUp(self):
        super(MoneyTransferTestCase, self).setUp()
        self.profiles = AutoFixture(
            models.UserProfile, generate_fk=['user']
        ).create(2)
        self.users = [p.user for p in self.profiles]
        # self.users = [User.objects.get(id=o.id) for o in self.users]

    def test_money_transfer_success(self):
        user_1 = self.users[0]
        user_2 = self.users[1]

        utils.handle_money_transfer(user_1, user_2, 1.50)

        # refresh_from_db is released in 1.8+
        user_1 = User.objects.get(id=user_1.id)
        user_2 = User.objects.get(id=user_2.id)
        self.assertEquals(user_1.profile.balance_rub, -1.50)
        self.assertEquals(user_2.profile.balance_rub, 1.50)


class BulkMoneyTransferTestCase(TestCase):
    """Тестирует логику перевода средств нескольким пользователям"""
    def setUp(self):
        super(BulkMoneyTransferTestCase, self).setUp()
        self.profiles = AutoFixture(
            models.UserProfile, generate_fk=['user'], field_values={
                'balance_rub': 0,
            }
        ).create(10)

        self.profiles[0].balance_rub = 100
        self.profiles[0].save()

        self.users = [p.user for p in self.profiles]

    def test_bulk_money_transfer_all_success_rv(self):
        user_src = self.users[0]
        users_dst = self.users[1:4]

        rv = utils.handle_bulk_money_transfer(user_src, users_dst, 30)
        self.assertTrue(all(rv.values()))

    def test_bulk_money_transfer_one_fail_rv(self):
        user_src = self.users[0]
        users_dst = self.users[1:4]

        del_id = users_dst[1].id
        User.objects.filter(id=del_id).delete()

        rv = utils.handle_bulk_money_transfer(user_src, users_dst, 30)
        self.assertFalse(all(rv.values()))

        rv_ = dict([(o.id, val) for o, val in rv.iteritems()])
        self.assertFalse(rv_[del_id])

    def test_bulk_money_transfer_even(self):
        user_src = self.users[0]
        users_dst = self.users[1:4]

        utils.handle_bulk_money_transfer(user_src, users_dst, 30)

        user_src = User.objects.get(id=user_src.id)
        users_dst = User.objects.filter(id__in=[o.id for o in users_dst]).all()

        self.assertEquals(user_src.profile.balance_rub, 70.0)

        for user_dst in users_dst:
            self.assertEquals(user_dst.profile.balance_rub, 10)

    def test_bulk_money_transfer_odd(self):
        user_src = self.users[0]
        users_dst = self.users[1:5]

        utils.handle_bulk_money_transfer(user_src, users_dst, 30)

        user_src = User.objects.get(id=user_src.id)
        users_dst = User.objects.filter(id__in=[o.id for o in users_dst]).all()

        self.assertEquals(user_src.profile.balance_rub, 70.0)

        for user_dst in users_dst:
            self.assertEquals(user_dst.profile.balance_rub, 7.5)

    def test_bulk_money_transfer_fractional_to_odd(self):
        user_src = self.users[0]
        users_dst = self.users[1:5]

        utils.handle_bulk_money_transfer(user_src, users_dst, 33.3)

        user_src = User.objects.get(id=user_src.id)
        users_dst = User.objects.filter(id__in=[o.id for o in users_dst]).all()

        self.assertEquals(float(user_src.profile.balance_rub), 66.7)

        for user_dst in users_dst:
            self.assertEquals(float(user_dst.profile.balance_rub), 8.32)


class BaseGetUserTestCase(TestCase):
    """Базовый класс для тестов utils.get_user и utils.get_users"""
    def setUp(self):
        super(BaseGetUserTestCase, self).setUp()
        self.profiles = AutoFixture(
            models.UserProfile, generate_fk=['user']
        ).create(5)
        self.users = [p.user for p in self.profiles]


class GetUserTestCase(BaseGetUserTestCase):
    """Тестирует получение пользователя по id, профилю"""
    def test_get_user_with_int(self):
        user = utils.get_user(self.users[0].id)
        self.assertEquals(user.id, self.users[0].id)

    def test_get_user_with_profile(self):
        user = utils.get_user(self.profiles[0])
        self.assertEquals(user.id, self.profiles[0].user.id)

    def test_get_user_with_user(self):
        user = utils.get_user(self.profiles[0].user)
        self.assertEquals(user.id, self.profiles[0].user.id)


class GetUsersTestCase(BaseGetUserTestCase):
    """Тестирует получение нескольких пользователей по id, профилю"""
    def test_get_users_with_different_values(self):
        users = sorted(self.users[0:4], key=lambda o: o.id)

        rv = utils.get_users([
            self.users[0].id,
            str(self.users[1].id),
            self.users[2].profile,
            self.users[3]
        ])

        rv = sorted(rv, key=lambda o: o.id)

        self.assertEquals(
            [o.id for o in users],
            [r.id for r in rv]
        )

    def test_get_users_by_username_or_tax_ok(self):
        username_user = utils.get_users_by_username_or_tax_id(
            self.users[0].username
        )

        tax_user = utils.get_users_by_username_or_tax_id(
            self.users[1].profile.tax_identifier
        )

        self.assertGreaterEqual(username_user.count(), 1)
        self.assertGreaterEqual(tax_user.count(), 1)


class TransferFormTestCase(TestCase):
    """Тестирует форму перевода средств"""
    def setUp(self):
        super(TransferFormTestCase, self).setUp()
        self.profiles = AutoFixture(
            models.UserProfile, generate_fk=['user']
        ).create(2)

        self.profiles[0].balance_rub = 10
        self.profiles[0].save()

        self.form_cls = forms.MoneyTransferForm

    def test_form_valid(self):
        form = self.form_cls({
            'amount': 1.0,
            'user_src': self.profiles[0].user_id,
            'tax_id_dst': self.profiles[1].tax_identifier,
        })

        self.assertTrue(form.is_valid())

    def test_form_invalid_min_amount(self):
        form = self.form_cls({
            'amount': 0,
            'user_src': self.profiles[0].user_id,
            'tax_id_dst': self.profiles[1].tax_identifier,
        })

        self.assertFalse(form.is_valid())
        self.assertIn('amount', form.errors)

    def test_form_invalid_not_enough_balance(self):
        assert self.profiles[0].balance_rub < 100

        form = self.form_cls({
            'amount': 100,
            'user_src': self.profiles[0].user_id,
            'tax_id_dst': self.profiles[1].tax_identifier,
        })

        self.assertFalse(form.is_valid())

    def test_form_invalid_tax_id(self):
        assert self.profiles[0].balance_rub < 100

        form = self.form_cls({
            'amount': 1,
            'user_src': self.profiles[0].user_id,
            'tax_id_dst': '0000',
        })

        self.assertFalse(form.is_valid())
        self.assertIn('tax_id_dst', form.errors)


class MessagesTestMixin(object):
    """Mixin для тестирование flash сообщений"""
    def setUp(self):
        messages.success = MagicMock()
        messages.warning = MagicMock()
        messages.error = MagicMock()
        super(MessagesTestMixin, self).setUp()


class BaseTransferViewTestCase(TestCase):
    """Базовый класс для тестирование view перевода средств"""
    def setUp(self):
        super(BaseTransferViewTestCase, self).setUp()
        self.profiles = AutoFixture(
            models.UserProfile, generate_fk=['user'], field_values={
                'balance_rub': 0,
            }
        ).create(10)

        self.profiles[0].balance_rub = 100
        self.profiles[0].save()


class TransferViewTestCase(BaseTransferViewTestCase):
    """Тестирует views перевода средств"""
    def test_valid_form_saved(self):
        data = {
            'amount': 1.0,
            'user_src': self.profiles[0].user_id,
            'tax_id_dst': self.profiles[1].tax_identifier,
        }

        rv = self.client.post(
            reverse('users:transfer'), data=data, follow=True
        )
        self.assertEquals(rv.status_code, 200)
        user = User.objects.get(id=self.profiles[0].user_id)
        self.assertEquals(user.profile.balance_rub, 99)

    def test_invalid_form_no_amount_not_saved(self):
        data = {
            'amount': '',
            'user_src': self.profiles[0].user_id,
            'tax_id_dst': self.profiles[1].tax_identifier,
        }

        rv = self.client.post(
            reverse('users:transfer'), data=data, follow=True
        )
        self.assertEquals(rv.status_code, 200)
        user = User.objects.get(id=self.profiles[0].user_id)
        self.assertEquals(user.profile.balance_rub, 100)

    def test_invalid_form_no_src_not_saved(self):
        data = {
            'amount': 1.0,
            'tax_id_dst': self.profiles[1].tax_identifier,
        }

        rv = self.client.post(
            reverse('users:transfer'), data=data, follow=True
        )
        self.assertEquals(rv.status_code, 200)
        user = User.objects.get(id=self.profiles[0].user_id)
        self.assertEquals(user.profile.balance_rub, 100)

    def test_invalid_form_no_dst_not_saved(self):
        data = {
            'amount': 1.0,
            'user_src': self.profiles[0].user_id,
            'tax_id_dst': None,
        }

        rv = self.client.post(
            reverse('users:transfer'), data=data, follow=True
        )
        self.assertEquals(rv.status_code, 200)
        user = User.objects.get(id=self.profiles[0].user_id)
        self.assertEquals(user.profile.balance_rub, 100)

    def test_empty_form_not_saved(self):
        data = {}

        rv = self.client.post(
            reverse('users:transfer'), data=data, follow=True
        )
        self.assertEquals(rv.status_code, 200)
        user = User.objects.get(id=self.profiles[0].user_id)
        self.assertEquals(user.profile.balance_rub, 100)

    def test_no_payload_not_saved(self):
        data = {}

        rv = self.client.post(
            reverse('users:transfer'), data=data, follow=True
        )
        self.assertEquals(rv.status_code, 200)
        user = User.objects.get(id=self.profiles[0].user_id)
        self.assertEquals(user.profile.balance_rub, 100)


class BaseTransferViewMessagesTestCase(MessagesTestMixin,
                                       BaseTransferViewTestCase):
    """
    Базовый класс для тестирования flash messages во вьюхе перевода средств
    """
    pass


class PartialTranferViewMessagesTestCase(BaseTransferViewMessagesTestCase):
    """
    Тестирует кейс, когда деньги переведены успешно лишь части пользователей
    """
    def setUp(self):
        super(PartialTranferViewMessagesTestCase, self).setUp()
        self.original_method = utils.handle_bulk_money_transfer
        utils.handle_bulk_money_transfer = MagicMock(return_value={
            self.profiles[1].user: False
        })

    def tearDown(self):
        super(PartialTranferViewMessagesTestCase, self).tearDown()
        utils.handle_bulk_money_transfer = self.original_method

    def test_form_partial_success_message_exists(self):
        data = {
            'amount': 1.0,
            'user_src': self.profiles[0].user_id,
            'tax_id_dst': self.profiles[1].tax_identifier,
        }

        rv = self.client.post(
            reverse('users:transfer'), data=data, follow=True
        )

        self.assertEquals(rv.status_code, 200)
        self.assertEqual(messages.warning.call_count, 1)
        self.assertIn(
            self.profiles[1].user.username,
            messages.warning.call_args[0][1]
        )


class TransferViewMessagesTestCase(BaseTransferViewMessagesTestCase):
    """Тестирует flash сообщений во вьюхе перевода средств"""
    def test_form_invalid_message_exists(self):
        data = {}

        rv = self.client.post(
            reverse('users:transfer'), data=data, follow=True
        )

        self.assertEquals(rv.status_code, 200)
        self.assertEqual(messages.error.call_count, 1)
        self.assertEqual(
            messages.error.call_args[0][1],
            views.TransferFormView.FORM_INVALID_MESSAGE
        )

    def test_form_success_message_exists(self):
        data = {
            'amount': 1.0,
            'user_src': self.profiles[0].user_id,
            'tax_id_dst': self.profiles[1].tax_identifier,
        }

        rv = self.client.post(
            reverse('users:transfer'), data=data, follow=True
        )

        self.assertEquals(rv.status_code, 200)
        self.assertEqual(messages.success.call_count, 1)
        self.assertEqual(
            messages.success.call_args[0][1],
            views.TransferFormView.SUCCESS_MESSAGE
        )


class TestUserSearchView(TestCase):
    """Тестирует вьюху для поиска пользователей"""
    def setUp(self):
        super(TestUserSearchView, self).setUp()
        self.profiles = AutoFixture(
            models.UserProfile, generate_fk=['user']
        ).create(5)
        self.users = [p.user for p in self.profiles]

    def test_search_by_username(self):
        resp = self.client.get(
            reverse('users:search'), {'term': self.users[0].username}
        )

        content = json.loads(resp.content)
        self.assertIn('results', content)
        results = content['results']
        self.assertIn('id', results[0])
        self.assertIn('text', results[0])

