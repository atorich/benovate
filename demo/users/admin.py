from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import UserProfile


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (UserProfileInline, )
    list_display = list(BaseUserAdmin.list_display) + [
        'tax_identifier', 'balance'
    ]

    def tax_identifier(self, obj):
        return obj.profile.tax_identifier

    def balance(self, obj):
        return obj.profile.balance_rub


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
