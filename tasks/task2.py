# coding=utf-8
"""
В чем отличие (i for i in arr) от [i for i in arr]?
Напишите генератор следующего вида

gen = reverse_gen([1, 2, 3, 4])

>> gen.next()

>> gen.next()

>> gen.next()

>> gen.next()

>> gen.next()

Traceback (most recent call last):

 File "<stdin>", line 1, in <module>

StopIteration
"""

answer = """
Отличие в том, что в первом случае создается генератор, во втором - список
"""


def reverse_gen(iterable):
    for i in xrange(len(iterable)-1, -1, -1):
        yield iterable[i]
