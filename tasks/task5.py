# coding=utf-8
"""
1. (Опционально) На входе - артикул (формат: последовательности цифр и букв

разделены точкой или пробелом). необходимо заменить точки на пробелы и пробелы на

точки с учетом всех возможных комбинаций.

Например, на входе артикул

1.2.3

На выходе должно быть:

1.2.3

1 2.3

1.2 3

1 2 3
"""
import itertools
import six


answer = """
Не совсем понятна формулировка задачи.
Может ли быть на входе артикул "1 2.3" или "1.2 3.4.5 6" итд.?
И если да, то что в таком случае меняется и как?
Некий тесткейс бы.

Ниже некоторые вариации на тему
"""


def replacer(value):
    if not isinstance(value, six.string_types):
        raise ValueError

    values = value.split('.')
    combs = [v for v in itertools.product(['.', ' '], repeat=len(values) - 1)]

    def mapper(a, b):
        if not b:
            b = ""
        return ''.join((a, b))

    for delimiters in combs:
        val = map(mapper, values, delimiters)
        yield ''.join(val)
