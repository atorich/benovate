# coding=utf-8
"""
Что такое декораторы? Для чего они нужны? Напишите кеширующий декоратор,
который можно применить к функции get_long_response и который, если результат
уже есть в кеше CACHE, вернет его, а если нет, то закеширует результат
выполнения функции в CACHE и вернет его (результат). Результат выполнения
функции get_long_response уникален для ее параметра user_id.
"""

answer = """
Хм.. функция (высшего порядка, ага), которая принимает в качестве аргумента
функцию, и возвращает другую функцию, внутри которой вызывается переданная.
Как-то так, если своими словами :)

Позволяет влиять на выполнению желаемой функции (из аргумента), например.
"""


class Cache(object):
    __cache = {}
    __slots__ = '__cache',

    @classmethod
    def get(cls, key):
        return cls.__cache[key]

    @classmethod
    def set(cls, key, value):
        cls.__cache[key] = value

    @classmethod
    def has(cls, key):
        return key in cls.__cache


def cache_decorator(cache):
    """
    Кеширующий декоратор
    :param cache: экземпляр для доступа к бэкенду кеша
    :return:
    """
    def wrapper(func):
        def inner(*args):
            cache_key = (func.__name__, args)

            if not cache.has(cache_key):
                value = func(*args)
                cache.set(cache_key, value)

            return cache.get(cache_key)
        return inner
    return wrapper
