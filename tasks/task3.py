# coding=utf-8
"""
Есть следующая функция:

    def myappend(a = [], num = 0):

         a.append(num)

        print a

Что будет происходить при выполнении следующего кода и почему:

    >>> a = [1,2,3]

    >>> myappend(a)

    >>> myappend()

    >>> myappend()
"""

answer = """
Потому что списки - изменяемые объекты.
Если передавать изменяемые объекты в качестве значений по умолчанию,
то постоянно будет использовать тот же самый объект

В первом случае мы в аргументе передали список, а
во втором и третьем случах мы аргумент пропустили и метод append() стал
вызываться для объекта по умолчанию.

Аналогичное поведение будет, например, если вместо списка будет словарь:
"""


def myupdate(d={}):
    d[len(d)] = 1
    return d

d = {'foo': 'bar'}
print myupdate(d)
print myupdate()
print myupdate()


def myappend(a=None, num=0):
    """Правильный myappend"""
    if a is None:
        a = []
    a.append(num)
    return a

a = [1, 2, 3]

print myappend(a)
print myappend()
print myappend()
