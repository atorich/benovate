# coding=utf-8
from unittest import TestCase

from . import task1, task2, task4, task5


class Task1TestCase(TestCase):
    """
    Напишите класс, который хранит список своих экземпляров и позволяет
    итерировать по ним.

        >>> a = Keeper()

        >>> b = Keeper()

        >>> for i in Keeper.list_instances():

        ...    print i

        <Keeper instance at 0x…
    """
    def test(self):
        task1.Keeper()
        task1.Keeper()

        for inst in task1.Keeper.iter_instances():
            self.assertIsInstance(inst, task1.Keeper)

        self.assertEquals(task1.Keeper.instances_count(), 2)


class Task2TestCase(TestCase):
    """
    В чем отличие (i for i in arr) от [i for i in arr]?
    Напишите генератор следующего вида

    gen = reverse_gen([1, 2, 3, 4])

    >> gen.next()

    >> gen.next()

    >> gen.next()

    >> gen.next()

    >> gen.next()

    Traceback (most recent call last):

     File "<stdin>", line 1, in <module>

    StopIteration
    """

    def test(self):
        gen = task2.reverse_gen([1, 2, 3, 4])
        self.assertEquals(gen.next(), 4)
        self.assertEquals(gen.next(), 3)
        self.assertEquals(gen.next(), 2)
        self.assertEquals(gen.next(), 1)

        with self.assertRaises(StopIteration):
            gen.next()


class Task4TestCase(TestCase):
    """
    Что такое декораторы? Для чего они нужны? Напишите кеширующий декоратор,
    который можно применить к функции get_long_response и который, если результат
    уже есть в кеше CACHE, вернет его, а если нет, то закеширует результат
    выполнения функции в CACHE и вернет его (результат). Результат выполнения
    функции get_long_response уникален для ее параметра user_id.
    """
    class CacheHit(Exception):
        pass

    def setUp(self):
        self.cache = task4.Cache()

    def test(self):
        counter = []

        @task4.cache_decorator(self.cache)
        def get_long_response(user_id):
            counter.append(1)
            return user_id * 1000
        
        val = get_long_response(1)
        self.assertEquals(val, 1000)
        val = get_long_response(2)
        self.assertEquals(val, 2000)
        val = get_long_response(3)
        self.assertEquals(val, 3000)

        val = get_long_response(1)
        self.assertEquals(val, 1000)
        val = get_long_response(2)
        self.assertEquals(val, 2000)

        # первые три раза вызывалась функция
        # крайние два раза значение берется из кеша
        # следовательно, в списке counter будет 3 элемента
        self.assertEquals(len(counter), 3)


class Task5TestCase(TestCase):
    """
    1. (Опционально) На входе - артикул (формат: последовательности цифр и букв

    разделены точкой или пробелом). необходимо заменить точки на пробелы и пробелы на

    точки с учетом всех возможных комбинаций.

    Например, на входе артикул

    1.2.3

    На выходе должно быть:

    1.2.3

    1 2.3

    1.2 3

    1 2 3
    """
    def test(self):
        """
        Не совсем понятна формулировка задачи.
        Может ли быть на входе артикул "1 2.3" или "1.2 3.4.5 6" итд.?
        И если да, то что в таком случае меняется и как?
        Некий тесткейс бы.

        Внутри некоторые вариации на тему
        """
        combinations = [s for s in task5.replacer('1.2.3')]
        self.assertEquals(combinations[0], '1.2.3')
        self.assertEquals(combinations[1], '1.2 3')
        self.assertEquals(combinations[2], '1 2.3')
        self.assertEquals(combinations[3], '1 2 3')
