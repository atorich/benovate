# coding=utf-8
"""
Напишите класс, который хранит список своих экземпляров и позволяет
итерировать по ним.

    >>> a = Keeper()

    >>> b = Keeper()

    >>> for i in Keeper.list_instances():

    ...    print i

    <Keeper instance at 0x…
"""


class Keeper(object):
    __instances = []

    def __new__(cls, *args):
        inst = super(Keeper, cls).__new__(cls, *args)
        cls.__instances.append(inst)
        return inst

    @classmethod
    def iter_instances(cls):
        return iter(cls.__instances)

    @classmethod
    def instances_count(cls):
        return len(cls.__instances)
